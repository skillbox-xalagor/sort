﻿#include <algorithm>
#include <vector>
#include <functional>
#include <iostream>
#include <set>
#include <string>

int main()
{
	std::vector<int> v(10);
	std::generate(v.begin(), v.end(), [n = 0]() mutable { return n++; });
	std::cout << "v: ";
	for (auto i : v)
		std::cout << i << ' ';
	std::cout << "\n";
	auto v0 = { 4, 2, 5, 1, 3 };
	std::vector<int>::iterator it;
	it = std::partial_sort_copy(v0.begin(), v0.end(), v.begin(), v.end());
	for (int i : v)
		std::cout << i << ' ';
	std::cout << '\n';

	std::vector<int> data = { 1, 2, 4, 5, 5, 6 };
	auto lower = std::lower_bound(data.begin(), data.end(), 4);
	std::cout << 4 << " <= ";
	lower != data.end()
		? std::cout << *lower << " at index " << std::distance(data.begin(), lower)
		: std::cout << "not found";
	std::cout << '\n';
	auto upper = std::upper_bound(data.begin(), data.end(), 4);
	std::cout << 4 << " >= ";
	upper != data.end()
		? std::cout << *upper << " at index " << std::distance(data.begin(), upper)
		: std::cout << "not found";
	std::cout << '\n';
	std::set<int> dataset = { 1, 2, 4, 5, 5, 6 };
	std::set<int>::iterator lowerset = dataset.lower_bound(4);
	std::cout << 4 << " <= ";
	std::cout << *lowerset;
	std::cout << '\n';
	std::set<int>::iterator upperset = dataset.upper_bound(4);
	std::cout << 4 << " >= ";
	std::cout << *upperset;
	std::cout << '\n';

	std::set<std::string> set1 = { "a", "b", "c", "d", "e", "f" };
	std::set<std::string> set2 = { "a", "b", "c" };
	std::cout << "includes:" << std::includes(set1.begin(), set1.end(), set2.begin(), set2.end()) << '\n';
	std::set<std::string> diff;
	std::set_difference(set1.begin(), set1.end(), set2.begin(), set2.end(), std::inserter(diff, diff.begin()));
	for (auto i : diff)
		std::cout << i << ' ';
}
